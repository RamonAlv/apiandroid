package com.example.api;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity{

    UserService userService;
    DatabaseHelper dbHandler = new DatabaseHelper(Login.this);
    public static final String EXTRA_MESSAGE = "app.activities.MESSAGE";

    private EditText EM, PW;

    private String Email, Password;
    public static String user_key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         EM = findViewById(R.id.email);
         PW = findViewById(R.id.password);
        final Button register = findViewById(R.id.btnRegister);

        userService = Connection.getServiceRemotee();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pantalla = new Intent(Login.this, Register.class);
                startActivity(pantalla);
            }
        });
    }

    public void OnLogin(View v){
        this.login();
    }

    public void login(){

        Email = EM.getText().toString();
        Password = PW.getText().toString();

        Call<ResponseBody> call = userService.login(Email, Password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String User = null;
                try{
                    if (response.isSuccessful()){
                        User = response.body().string();

                        String [] Token = User.split("[, , :]");
                        user_key = "";

                        for(int i = 0; i<Token[1].length(); i++){
                            if(i!=0 && i != Token[1].length()-1){
                                user_key += Token[1].charAt(i);
                            }
                        }

                        if (User.length() < 2360) {
                            Intent viewList = new Intent(Login.this, Lista.class);
                            Toast.makeText(getApplicationContext(), "Bienvenido..", Toast.LENGTH_SHORT).show();
                            startActivity(viewList);
                        }else
                            Toast.makeText(getApplicationContext(), "Usuario incorrecto..", Toast.LENGTH_SHORT).show();


                    }
                }catch (IOException e){}
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
