package com.example.api;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface ReportService {

    @GET("reports")
    Call<ResponseBody> getReports(
            @Header("Content-Type") String Content_Type,
            @Header("Authorization") String Authorization
    );

    @POST("reports")
    Call<Report> addReport(
            @Header("Content-Type") String Content_Type,
            @Header("Authorization") String Authorization,
            @Body Report report
    );

    @GET("reports/{id}")
    Call<ResponseBody> getByIdReport(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth,
            @Path("id") String id
    );

    @GET
    Call<ResponseBody> getImg(
            @Url String URL
    );

    @PUT("reports/{id}")
    Call<Report> updateReport(@Path("id") int id, @Body Report report);

    @DELETE("reports/{id}")
    Call<Report> deleteReport(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth,
            @Path("id") int id
    );

    @GET("logout")
    Call<ResponseBody> logout(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth
    );
}
