package com.example.api;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Lista extends AppCompatActivity implements DenunciaAdapter.AdapterButtonsListener{

    ReportService reportService;

    private ListView listView;
    private DenunciaAdapter denunciaAdapter;
    private  ArrayList<Report> denuncia;
    public static final String EXTRA_MESSAGE = "app.activities.MESSAGE";

    public static String USERID = "";
    DatabaseHelper db = new DatabaseHelper(Lista.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //OBTENER Token DEL USUARIO QUE TRAIGO DEL LOGI

        reportService = Connection.getServiceRemote();
        System.out.println(Login.user_key);

        listView = findViewById(R.id.ViewList);
        denuncia = new ArrayList<>();
        denunciaAdapter = new DenunciaAdapter(getApplicationContext(), denuncia);
        denunciaAdapter.setListener(this);
        listView.setAdapter(denunciaAdapter);
        //get
        Call<ResponseBody> call = reportService.getReports("application/json", "Bearer "+Login.user_key);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String[] datos;
                    String [] dato;
                    ArrayList<String> data = new ArrayList<>();
                    datos = response.body().string().split(":",2);
                    datos[0] = "";
                    for(int i = 0; i<datos[1].length(); i++){
                        if(datos[1].charAt(i) != '{' && datos[1].charAt(i) != '}' &&
                                datos[1].charAt(i) != '[' && datos[1].charAt(i) != ']' &&
                                datos[1].charAt(i) != '"' && datos[1].charAt(i) != 92){
                            datos[0] += datos[1].charAt(i);
                        }
                    }
                    datos = datos[0].split(",");
                    if (response.code() == 200) {
                        for(String i : datos){
                            dato = i.split(":");
                            if (dato[0].equals("id")){
                                data.add(dato[1]);
                            }else {
                                data.set(data.size()-1, data.get(data.size()-1) + ":" + dato[1]);
                            }
                        }
                        for(String i : data){
                            dato = i.split(":");
                            denuncia.add(new Report(dato[0], dato[1], dato[2], dato[3]));
                            denunciaAdapter.notifyDataSetChanged();
                        }
                    }

                }catch (IOException e){}//ram@alv.com

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Error " + t);
            }
        });
        //getfin
        //Boton Flotante
        final FloatingActionButton addItem = findViewById(R.id.fab);
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pantalla2 = new Intent(Lista.this, Form.class);
                startActivity(pantalla2);

            }
        });
        //BotonFlotanteFin
        //logout
        final Button close = findViewById(R.id.btnCerrar);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<ResponseBody> call = reportService.logout("application/json", "Bearer "+Login.user_key);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getApplicationContext(), "Sesion Finalizada...", Toast.LENGTH_SHORT).show();
                            Intent pantalla2 = new Intent(Lista.this, Login.class);
                            startActivity(pantalla2);
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
        //logoutfin
    }


    @Override
    public void onButtonEditClickListener(int position, Report denuncia) {
        /*String reportID = denuncia.getId();
        System.out.println("ID Report: "+reportID);
        /*Intent pantalla2 = new Intent(Lista.this, Details.class);
        pantalla2.putExtra(EXTRA_MESSAGE,reportID);
        startActivity(pantalla2);*/
        System.out.println("-------------------------------------");
        System.out.println("ID: "+denuncia.getId());
        System.out.println("Title: "+denuncia.getTitle());
        System.out.println("Description: "+denuncia.getDescription());
        System.out.println("--------------------------------------");

        String reportID = denuncia.getId();
        System.out.println("ID Report: "+reportID);
        Intent pantalla2 = new Intent(Lista.this, Details.class);
        pantalla2.putExtra(EXTRA_MESSAGE, reportID);
        startActivity(pantalla2);
    }

    @Override
    public void onButtonDeleteClickListener(int position, Report denuncia) {

    }
}



