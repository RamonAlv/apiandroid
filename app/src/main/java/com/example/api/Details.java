package com.example.api;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Details extends AppCompatActivity {

    private ImageView imgset;
    public static String URLImg;
    ReportService reportService;
    ReportService ImgURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        reportService = Connection.getServiceRemote();
        ImgURL = Connection.getUrl();

        final TextView title = findViewById(R.id.txtTitle);
        final TextView description = findViewById(R.id.txtDescription);
        final TextView image = findViewById(R.id.txtImage);
        final Button back = findViewById(R.id.btnDelete);

        imgset = findViewById(R.id.imgset);

        Intent reportID = getIntent();
        String ID = reportID.getStringExtra(Login.EXTRA_MESSAGE);

        System.out.println(ID);
        //getbid sin imagen
        Call<ResponseBody> call = reportService.getByIdReport("application/json", "Bearer "+Login.user_key, ID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try{
                    String[] datos;
                    String [] dato;
                    ArrayList<String> data = new ArrayList<>();
                    datos = response.body().string().split(":",2);
                    datos[0] = "";
                    for(int i = 0; i<datos[1].length(); i++){
                        if(datos[1].charAt(i) != '{' && datos[1].charAt(i) != '}' &&
                                datos[1].charAt(i) != '[' && datos[1].charAt(i) != ']' &&
                                datos[1].charAt(i) != '"' && datos[1].charAt(i) != 92){
                            datos[0] += datos[1].charAt(i);
                        }
                    }
                    datos = datos[0].split(",");
                    if (response.code() == 200) {
                        for (String i : datos) {
                            dato = i.split(":");
                            data.add(dato[1]);
                        }
                    };
                    title.setText(data.get(1));
                    description.setText(data.get(2));
                    URLImg = data.get(3);
                    SetImagen();
                    image.setText(data.get(3));
                }catch (IOException e){}

                title.setEnabled(false);
                description.setEnabled(false);
                image.setEnabled(false);
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Error " + t);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pantalla2 = new Intent(Details.this, Lista.class);
                startActivity(pantalla2);

            }
        });
    }

    public void SetImagen(){

        Call<ResponseBody> callImg = ImgURL.getImg(URLImg);
        System.out.println("URL => " + URLImg);
        callImg.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    if(response.body() != null){
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        imgset.setImageBitmap(bmp);
                    }else
                        System.out.println("No Funciona response.body");
                }else
                    System.out.println("No Funciona response");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Error T => " + t.toString());
            }
        });
    }
}
